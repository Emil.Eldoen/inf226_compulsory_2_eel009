# inf226_compulsory_2_eel009

# README
***
# Task 1:
***
The secret key is a set string "mY s3kritz" and should be randomized rather than set to a constant string. (line 27) this could allow someone to impersonate a different user by stealing their secret key, setting up for a man in the middle attack. Should be fixed by generating a random value and sent to a database for verification.

users and passwords are stored in plain text (line 37-39) this would allow anyone to impersonate a user by simply stealing their username and password. Should be hashed and stored in a database.

There is no password check, should fetch a password from user and hash the input password, check against hashed/salted password from database.

The database has no user table created to store user information. Useful for user auth.

When sending a message there is no checking if there is a valid user, can be fixed by implementing a check against the current user somehow? Maybe session cookie?

Changes made:
Changed the session token to a randomly generated token using Pythin secrets.
Imported werkzeug security packages for password hashing
Expanded database to allow for meta data about messages and response ids. Added authentication table to store usernames and passwords
Users are now stored in a database with salted password, considering they are "test users" they have simply been initialized and are expected to be removed before code is put online
***

# Part 2B

**Task1**
---
TL;DR for task 1 in part 2A:
There were some security issues, such as the db being stored as a table only, no password check and no checks being made towards db.
On top of that session cookie was stored as a simple string and passwords were not stored with hashing.

---
**Task 2**
The application features the following:
A login page for users to log into the messaging application
A messaging html page, allowing users to see all messages sent by them or to search for specific messages sent by them.
They can also send further messages and reply to different messages so long as they have the message ID.

---
**Task 3**
Demoing:
To demo the application, run the command "flask run" from your terminal (if it doesn't work in the ide like VSCode, simply open your terminal and navigate to the "login" folder, then run flask from the terminal in that folder)

To access the messaging page, the app.py itializes 2 users bob and alice, to login write "alice" and "password123" or "bob" and "bananas" (the login is **CASE SENSITIVE** so make sure to write exactly as written)
Once in, try sending a message by either clicking "send" or you can write your own custom message in the message field and click the "send!" button to register the message to the database.

To search for a specific message, simply write in the search form, this will find any messages containing a specific string.
ID search was not implemented due to time constraint. Please see the final section for the issues still in the application as well as assumptions made.

---
**Task 4**
Technical details and implementations:
This system uses app.py to gather information sent from a html form to perform functions and give users a response.
Any information written into the forms on the index.html and login.html page will be sent to app.py for processing and checking whether it be login, message sending or message searching.

To do this the flask library is used to handle most of the issues pretaining to logging in, sessions, sending and retrieving information as well as logging out. It is also used to generate the secret key for a session.

Due to time constraints passwords are hashed and stored in the database, but the time it would take to implement a check and test towards the database was not possible within the deadline. This could have been solved somewhat easy via a library like SQLAlchemy.

Hashing is handled by the werkzeug.security import via generate_password_hash, 
If the implementation of fetching registered user info password could have been checked via check_password_hash, 
and passwords would've been salted by gen_salt before being sent to the database.
**NOTE:** When implementing the proper password database check, make sure to hash the input password before running check_password_hash.

---
**Task 5:**
# Who might attack the application?
The application can be attacked by many individuals and groups, such as hacktivists, hackers people looking for user information (such as names and passwords), hacker(s) who may wish to cause damage to the deployed messaging system by trying to delete stored information etc.

---
# What can an attacker do?
An attacker has many attack vectors to employ, among others social engineering such as phising, man in the middle attacks, and possibly injection, and exploiting our insecre design (some of which has been fixed). 

**Insecure design:**
One very big attack vector in the current application is the initalization of default users (these are assumed to be removed after the code exits production), making it very easy to gain access (see assumptions for an explanation of why these users are here).
It also does not help we have a dictionary of users which should be deleted once user & password check against DB is fully implemented

**Phising:** 
A social engineering strategy hackers employ to trick users to give their information. This can be done via links taking them to a login page similar to the service they try to gain access to, or by asking them for information that can be used to reset passwords (security questions, email addresses etc).

**Man in the middle attack:** 
Happens when a hacker/malicious party manages to pretend to be a different user to communicate with someone else or simply read their messages.
Technically possible due to an oversight in the HTML.
A user can simply edit the name used in the "From" part of the form to appear as a different user (see assumptions for why this hasn't been fixed).

**SQLInjection:**
Injection might be possible, however in my testing I was given syntax errors as the metadata was sent last, meaning too many parameters were sent in the event of an attack directly via the input field. However prepared statements should still be implemented.

**XSS attacks:**
All variables are sent via encodeURI which should help somewhat in ensuring scripting does not happen.
None of the input from user is set directly into the HTML, as such cross site scripting shouldn't be an option.
Sending in HTML however, does make the output seem rather odd when displaying messages.

**Cross-site request forgery:**
Sending a message on a different application's behalf shouldn't be possible due to the session cookie being used.

---
# What damage could be done in regards to *confidentiality*, *integrity* and *avaialbiltiy*?

---
**Confidentiality:** 
A user could acquire the information of other users via phising and man-in-the-middle attacks by sending messages as a different user input in the HTML.
It might also be possible to acquire user info due to the application's lack of prepared statements, however this might be somewhat mitigated by the order we are sending/expecting the data.
The information and passwords (on the database anyway), user passwords are stored hashed and should provide some security, ideally they would also be salted.

---
**Integrity:**
The informaton should be difficult to change once it is in the database, however due to insecure HTML design it is very easy to impersonate users or make entirely new usernames input into the database. It is also technically possible to respond to an invalid id.

---
**Availabiltiy:**
This is a difficult question to answer, as no information in regards to the scope of this project when it comes to hardware, memory or storage.
As such, assuming everything were to be run from a single laptop it would be INCREDIBLY easy to commit a DOS attack by spamming requests and messages resulting in memory, storage and other hardware resources being used up rather quickly.

**Tracability:**
It might be somewhat difficult to find exactly which user did what, however it would've been simple to store the user and their token on the DB as a pair together with the date they logged in. However, due to time constraints this was not implemented.
Having a relation between when a user logs in, what messages they send combined with a check on the token, it would be rather simple to trace what users do and in the case of an attack, find which user was compromised and then shut them down.

The main attack vectors are our input fields, insecure db queries,  user checks, default users generated, phising.

**What has been done?**
some of the work towards protecting the site has been done but not fully finished.
User passwords and information have mostly been moved from app.py to the database
Password check has been implemented (albeit not fully for database checking)
Passwords are hashed (but not salted)
When searching for messages the only user shows up is the one listed in the "from" part of the form (see assumptions for reasoning for lack of fix)
User input is encoded and sent to app.py, nothing is being processed on the user-side apart from the timestamp metadata.
Nothing has been done to combat phising, this is assumed to be a different responsibility for another department.

---
**Limits for attackers and our protection:**

**Attacker limits:**
There are some limits to what an attacker can do to this applicatio. Getting passwords and other info from the database should be a little more difficult than just SQL-injection. Session token makes it harder to perform CSRF, XSS shouldn't be possible as while it makes their message output seem weird, that seems to be the only side-effect of the attempt.
Our biggest exploit are the default users, which are assumed to be removed before the code enters actual production, as well as the insecure password check. Some functions have been made to fetch the information to check db for users and password, however the check itself has not been implemented (again) due to time constraint.

**Our protection limits:**.
There are some limits to what one can reasonably protect against as well, and this is users doing things wrong.
While it is possible to give out information campaigns and reminders to users (i.e "never give your password or personal information to staff", "make sure to use secure passwords", 2 factor auth etc) phising will always be problematic as anyone can be tricked or have a lapse of judgement.

---
**Access control module:**
The access control module used is of a attribute-based kind. The users request to log in and are given access depending on their input.
A user cannot edit or see other user's messages (in theory), and cannot send messages to other users once the HTML exploit has been fixed.

---
**How to know if security is good enough?**
The only way to know for sure is to run tests and possible exploits, evaluating the security mesaures and logging information on what is being done to the system.

While this application doesn't feature much logging, this is mostly due to time contraints once more.

Tracing what users do, what operations are done and when can help build a better picture of attacks, which kind and from where and when they come.

This can be done via logging:
The logging should be stored in a database or some other system separate from the application.

Information should be stored in sets that makes sense and can be read (i.e an error should not be stored in the same table as a sucessful login)

Information worth considerign to store:
Errors & failures such as:
authentication failures
errors as a result of validating input or other data
errors of users trying to acquire access to things they shouldn't
session managment failures (such as a cookie identification error)
errors thrown by the application
errors on systems such as servers

other things to log:
Changes to data (i.e changes to the database)
unexpected behaviours
Code/file/memory changes to the db or other parts of the application
Undefined values that are expected to be defined
Timestamps of when actions, errors, input occurs

# Some final notes:
This project was done by 1 person, given that the limited time and workforce I had to make a few assumptions of tasks that would have been given to hypothetical coworkers.

User registration form and in general user registration is assumed to be handled by someone else, hence the db is initialized with users for the sake of testing but assumed to be removed after code enters production.

HTML errors and insecure design is also assumed to have been handled by a hypothetical corworker, as most of my focus went into app.py.

**Things I would have done differently:**
If I had more time:
 I would have rebuilt how SQL is handled, as there is a useful python library called SQLAlchemy that makes it easier to send queries and set up databases.
 Improved the design of the page to make it more user friendly by making the login form more defined and split up
 Split up functionality in app.py to make it easier to keep track of the code and what is being done where (i.e a inputProcessing.py for acquiring inputs and processing outputs, a query.py for handling db queries, setup.py for the intial setup of testdata that could easily be deleted later)

# A few things that were not implemented:

Password checking towards db, if this project were to be handed to someone else I would highly encourage them to implement it.
Password salting, the function is available but not used
Prepared statements: would recommend adding this
Remove test-data from app.py once registration form has been made and is usable
